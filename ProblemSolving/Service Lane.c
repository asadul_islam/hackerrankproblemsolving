#include <stdio.h>
#include <stdlib.h>

int* serviceLane(int n, int cases_rows, int cases_columns, int* width, int** cases) {
    int* result = (int *)malloc(cases_rows * sizeof(int));
    for(int i = 0; i < cases_rows; i++){
        int mx = width[cases[i][0]];
        for(int j = cases[i][0]; j <= cases[i][1]; j++){
            if(width[j] < mx){
                mx = width[j];
            }
        }
        result[i] = mx;
    }
    
    return result;
}

int main()
{
    int n, t;

    scanf("%d %d", &n, &t);

    int* width = malloc(n * sizeof(int));
    for (int i = 0; i < n; i++) {
        scanf("%d", &width[i]);
    }

    int** cases = malloc(t * sizeof(int*));

    for (int i = 0; i < t; i++) {
        cases[i] = malloc(2 * (sizeof(int)));
        scanf("%d %d", &cases[i][0], &cases[i][1]);
    }

    int* result = serviceLane(n, t, 2, width, cases);
    for (int i = 0; i < t; i++) {
        printf("%d", *(result + i));

        if (i != t - 1) {
            printf("\n");
        }
    }
    printf("\n");

    return 0;
}

