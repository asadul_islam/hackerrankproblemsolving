#include <stdio.h>
#include <stdlib.h>

int viralAdvertising(int n) {
    int liked = 0, total_liked = 0, shared = 5;
    for(int i = 1; i <= n; i++){
        liked = shared / 2;
        total_liked += liked;
        shared = liked * 3;
    }
    return total_liked;
}

int main()
{
    int n;
    scanf("%d", &n);

    int result = viralAdvertising(n);
    printf("%d\n", result);
    return 0;
}
