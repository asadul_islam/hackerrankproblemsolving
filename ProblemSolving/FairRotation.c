#include <stdio.h>
#include <stdlib.h>

int fairRations(int B_count, int* B) {
    int cnt = 0;
    for(int i = 0; i < B_count - 1; i++){
        if(B[i] % 2 == 1){
            cnt += 2;
            B[i]++;
            B[i + 1]++;
        }
    }

    if(B[B_count - 1] % 2 == 1){
        return -1;
    }
    
    return cnt;
}

int main()
{
    int N;
    int* B;
    scanf("%d", &N);
    B = malloc(N * sizeof(int));

    for (int i = 0; i < N; i++) {
        scanf("%d", &B[i]);
    }

    int result = fairRations(N, B);

    if(result != -1){
        printf("%d\n", result);
    }
    else{
        printf("NO\n");
    }

    return 0;
}


