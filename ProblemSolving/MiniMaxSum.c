#include <stdio.h>
#include <stdlib.h>

void miniMaxSum(int arr_count, int* arr) {
    long long int mx = 0, mn = 0;
    for(int i = 0; i < arr_count; i++){
        for(int j = i + 1; j < arr_count; j++){
            if(*(arr + j) < *(arr + i)){
                int tmp = *(arr + i);
                *(arr + i) = *(arr + j);
                *(arr + j) = tmp;
            }
        }
    }

    for(int i = 0; i < 4; i++){
        mn += *(arr + i);
    }
    
    for(int i = arr_count - 1; i >= arr_count - 4; i--){
        mx += *(arr + i);
    }
    
    printf("%lld %lld\n", mn, mx);
}

int main()
{
    int* arr = malloc(5 * sizeof(int));
    int arr_count = 5;
    
    for(int i = 0; i < 5; i++){
        scanf("%d", &arr[i]);
    }

    miniMaxSum(arr_count, arr);

    return 0;
}

