#include <stdio.h>
#include <stdlib.h>

int pickingNumbers(int a_count, int* a) {
    int mx = 0;
    for(int i = 0; i < a_count; i++){
        for(int j = i + 1; j < a_count; j++){
            if(a[j] < a[i]){
                int tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
            }
        }
    }

    for(int i = 0; i < a_count; i++){
        int tmp_count = 1;
        for(int j = i + 1; j < a_count; j++){
            if(a[j] - a[i] > 1){
                break;
            }
            tmp_count++;
        }

        if(tmp_count > mx){
            mx = tmp_count;
        }
    }
    
    return mx;
}

int main()
{
    int n;
    scanf("%d", &n);

    int* a = malloc(n * sizeof(int));
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
    }

    int result = pickingNumbers(n, a);
    printf("%d\n", result);
    return 0;
}

