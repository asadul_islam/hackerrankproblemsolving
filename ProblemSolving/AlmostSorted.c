#include <stdio.h>
#include <stdlib.h>

int isSorted(int arr_count, int* arr){
    for(int i = 1; i < arr_count; i++){
        if(arr[i] < arr[i - 1])
            return 0;
    }
    return 1;
}

void swap(int *a, int *b){
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void almostSorted(int arr_count, int* arr) {
    if(isSorted(arr_count, arr)){
        printf("yes\n");
        return;
    }

    int sawp_with = -1;
    for(int i = 0; i < arr_count - 1; i++){
        if(arr[i + 1] < arr[i]){
            if(sawp_with < 0){
                sawp_with = i;
            }
            swap(&arr[sawp_with], &arr[i + 1]);
            if(isSorted(arr_count, arr)){
                printf("yes\n");
                printf("swap %d %d\n", sawp_with + 1, i + 2);
                return;
            }
            swap(&arr[sawp_with], &arr[i + 1]);
            sawp_with = i;
        }
    }

    int reverse_start = -1, reverse_end = -1;

    for(int i = 0; i < arr_count - 1; i++){
        if(arr[i] > arr[i + 1]){
            if(reverse_start == -1){
                reverse_start = i;
            }
            reverse_end = i + 1;
        }
        else if(reverse_start != -1){
            break;
        }
    }

    for(int i = reverse_start, j = reverse_end; i < j; i++, j--){
        swap(&arr[i], &arr[j]);
    }

    if(isSorted(arr_count, arr)){
        printf("yes\nreverse %d %d\n", reverse_start + 1, reverse_end + 1 );
        return;
    }

    printf("no\n");
}

int main()
{
    int* arr;
    int arr_count;
    scanf("%d", &arr_count);

    arr = malloc(arr_count * sizeof(int));
    for(int i = 0; i < arr_count; i++){
        scanf("%d", &arr[i]);
    }

    almostSorted(arr_count, arr);

    return 0;
}


