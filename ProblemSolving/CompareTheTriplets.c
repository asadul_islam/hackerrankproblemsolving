#include <stdio.h>
#include <stdlib.h>

int* compareTriplets(int a_count, int* a, int b_count, int* b, int* result_count) {
    int* result = (int *)malloc(2 * sizeof(int));
    int alicePoint = 0, bobPoint = 0;
    for(int i = 0; i < a_count; i++){
        if(*(a + i) > *(b + i)){
            alicePoint++;
        }
        else if(*(a + i) < *(b + i)){
            bobPoint++;
        }
    }
    *result_count = 2;
    result[0] = alicePoint;
    result[1] = bobPoint;
    
    return result;
}

int main()
{
    int* a = malloc(3 * sizeof(int));

    for (int i = 0; i < 3; i++) {
        scanf("%d", &a[i]);
    }

    int a_count = 3;
    int* b = malloc(3 * sizeof(int));

    for (int i = 0; i < 3; i++) {
        scanf("%d", &b[i]);
    }

    int b_count = 3;
    int result_count;
    int* result = compareTriplets(a_count, a, b_count, b, &result_count);

    for (int i = 0; i < result_count; i++) {
        printf("%d", *(result + i));

        if (i != result_count - 1) {
            printf(" ");
        }
    }

    printf("\n");
    return 0;
}

