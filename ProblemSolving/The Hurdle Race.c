#include <stdio.h>
#include <stdlib.h>

int hurdleRace(int k, int height_count, int* height) {
    int mx = 0;
    for(int i = 0; i < height_count; i++){
        if(height[i] > mx){
            mx = height[i];
        }
    }
    if(mx > k){
        return mx - k;
    }
    return 0;
}

int main()
{
    int n, k;
    int* height; 

    scanf("%d %d", &n, &k);
    height = malloc(n * sizeof(int));
    for(int i = 0; i < n; i++){
        scanf("%d", &height[i]);
    }

    int result = hurdleRace(k, n, height);
    printf("%d\n", result);
    return 0;
}
