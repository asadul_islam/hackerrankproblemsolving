#include <stdio.h>
#include <stdlib.h>

int lower_bound(int scores_count, int* scores, int search_score, int* ranks){
    int end, begin, mid;
    begin = 0;
    end = scores_count - 1;

    while(begin <= end){
        mid = (end - begin )/2 + begin;
        if(search_score == scores[mid] && scores[mid - 1] > search_score){
            return ranks[mid];
        }
        if(search_score >= scores[mid]){
            end = mid - 1;
        }
        else{
            begin = mid + 1;
        }
    }

    return ranks[begin];
}

int* climbingLeaderboard(int scores_count, int* scores, int alice_count, int* alice) {
    int* res = (int *)malloc(alice_count * sizeof(int));
    int* ranks = (int *)malloc((scores_count + 1) * sizeof(int));
    int k = 1;
    ranks[0] = k;
    for(int i = 1; i < scores_count; i++){
        if(scores[i] == scores[i - 1]){
            ranks[i] = k;
        }
        else{
            ranks[i] = ++k;
        }
    }
    ranks[scores_count] = ++k;

    for(int i = 0; i < alice_count; i++){
        int rank = lower_bound(scores_count, scores, alice[i], ranks);
        res[i] = rank;
    }
    return res;
}

int main()
{
    int scores_count;
    scanf("%d", &scores_count);
    int* scores = malloc(scores_count * sizeof(int));

    for (int i = 0; i < scores_count; i++) {
        scanf("%d", &scores[i]);
    }

    int alice_count;
    scanf("%d", &alice_count);
    int* alice = malloc(alice_count * sizeof(int));

    for (int i = 0; i < alice_count; i++) {
        scanf("%d", &alice[i]);
    }

    int* result = climbingLeaderboard(scores_count, scores, alice_count, alice);

    for (int i = 0; i < alice_count; i++) {
        printf("%d \n",result[i] );
    }
    return 0;
}
