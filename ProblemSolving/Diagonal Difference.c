#include <stdio.h>
#include <stdlib.h>

int diagonalDifference(int n, int** arr) {
    int left = 0, right = 0;

    for(int i = 0; i < n; i++){
        left += *(*(arr + i) + i);
    }

    for(int i = 0, j = n - 1; i < n; i++, j--){
        right += *(*(arr + i) + j);
    }

    int diff = (left > right) ? left - right : right - left;

    return diff;
}

int main()
{
    int n ;
    int** arr;

    scanf("%d", &n);
    arr = malloc(n * sizeof(int*));
    for(int i = 0; i < n; i++){
        arr[i] = malloc(n * sizeof(int));
        for(int j = 0; j < n; j++){
            scanf("%d", &arr[i][j]);
        }
    }

    int result = diagonalDifference(n, arr);
    printf("%d\n", result);

    return 0;
}

