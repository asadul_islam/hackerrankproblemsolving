#include <stdio.h>
#include <stdlib.h>

int hourglassSum(int n, int **arr) {
  int mx = 0;
  for (int i = 1; i < n - 1; i++) {
    for (int j = 1; j < n - 1; j++) {
      int v = arr[i - 1][j - 1] + arr[i - 1][j] + arr[i - 1][j + 1] +
              arr[i][j] + arr[i + 1][j - 1] + arr[i + 1][j] + arr[i + 1][j + 1];
      if (i == 1 && j == 1) {
        mx = v;
      }
      if (v > mx) {
        mx = v;
      }
    }
  }
  return mx;
}

int main()
{
    int** arr = malloc(6 * sizeof(int*));

    for (int i = 0; i < 6; i++) {
        *(arr + i) = malloc(6 * (sizeof(int)));
        for(int j = 0; j < 6; j++){
            scanf("%d", &arr[i][j]);
        }
    }

    int result = hourglassSum(6, arr);
    printf("%d\n", result);
    return 0;
}
