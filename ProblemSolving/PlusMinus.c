#include <stdio.h>
#include <stdlib.h>

void plusMinus(int arr_count, int* arr) {
    int pos = 0, neg = 0, zero = 0;
    for(int i = 0; i < arr_count; i++){
        if(*(arr + i) > 0){
            pos++;
        }
        else if(*(arr + i) < 0){
            neg++;
        }
        else{
            zero++;
        }
    }
    
    double posFraction, negFraction, zerFraction;
    posFraction = pos/(double)arr_count;
    negFraction = neg/(double)arr_count;
    zerFraction = zero/(double)arr_count;
    
    printf("%.6lf\n%.6lf\n%.6lf\n", posFraction, negFraction, zerFraction);
}

int main()
{
    int n;
    scanf("%d", &n);
    int* arr = malloc(n * sizeof(int));
    for(int i = 0; i < n; i++){
        scanf("%d", &arr[i]);
    }
    plusMinus(n, arr);

    return 0;
}
