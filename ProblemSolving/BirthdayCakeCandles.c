#include <stdio.h>
#include <stdlib.h>


char* readline();
char** split_string(char*);


int birthdayCakeCandles(int ar_count, int* ar) {
    int cnt = 0, mx = *ar;
    for(int i = 1; i < ar_count; i++){
        if(*(ar + i) > mx){
            mx = *(ar + i);
        }
    }
    for(int i = 0; i < ar_count; i++){
        if(*(ar + i) == mx){
            cnt++;
        }
    }
    return cnt;
}

int main()
{
    int ar_count;
    int* ar;
    scanf("%d", &ar_count);
    ar = malloc(ar_count * sizeof(int));
    for(int i = 0; i < ar_count; i++){
        scanf("%d", &ar[i]);
    }

    int result = birthdayCakeCandles(ar_count, ar);

    printf("%d\n", result);
    return 0;
}
